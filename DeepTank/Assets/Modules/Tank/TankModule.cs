﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankModule : ModuleScript {

    Player player1, player2;
    

    Tank tank;
    int maxFishIn = 3;
    int currentFishIn = 0;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        player1 = submarine.transform.GetChild(0).GetComponent<Player>();
        player2 = submarine.transform.GetChild(1).GetComponent<Player>();
        tank = transform.GetChild(4).GetComponent<Tank>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
       
        //player1
        if (tank.p1Inside)
        {
            if (player1.HasAnimalInHand())
            {
                if (Input.GetKeyDown(player1.handleObject))
                {
                    if (currentFishIn < maxFishIn)
                    {
                        //deixar el peix a dins
                        FindObjectOfType<ManagerGame>().SetPoints(player1.GetAnimalPoints());
                        player1.SetAnimalInAquarium(this.gameObject.transform);
                        currentFishIn++;
                        
                    }
                }
            }

        }

        //player2
        if (tank.p2Inside)
        {
            if (player2.HasAnimalInHand())
            {
                if (Input.GetKeyDown(player2.handleObject))
                {
                    if(currentFishIn < maxFishIn)
                    {
                        //deixar el peix a dins
                        FindObjectOfType<ManagerGame>().SetPoints(player2.GetAnimalPoints());
                        player2.SetAnimalInAquarium(this.gameObject.transform);
                        currentFishIn++;
                    }

                }
            }

        }
        
    }

    public override void ResetModule()
    {
        Debug.Log("Module reseted from child class");
    }
}
