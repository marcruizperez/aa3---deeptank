﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleScript : MonoBehaviour {

    //[HideInInspector] public bool top, right, bot, left; //if true there is module
    public bool[] adjacent; //if true there is module attached
    protected submarineScript submarine;

    // Use this for initialization
    protected virtual void Start () {
        //adjacent = new bool[] { false, false, false, false }; this works
        submarine = GameObject.Find("Submarine").GetComponent<submarineScript>();
        //Debug.Log("Start function from ParentClass");
    }
	
	// Update is called once per frame
	protected virtual void Update () {
        
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    UpdateWallTag();
        //}
	}

    public virtual void ResetModule()
    {

    }

    public void UpdateWallTag()
    {
        Transform top, right, bot, left;
        top = transform.GetChild(0);
        right = transform.GetChild(1);
        bot = transform.GetChild(2);
        left = transform.GetChild(3);

        if (adjacent[0]) top.tag = "Through"; else top.tag = "Untagged";
        if (adjacent[1]) right.tag = "Through"; else right.tag = "Untagged";
        if (adjacent[2]) bot.tag = "Through"; else bot.tag = "Untagged";
        if (adjacent[3]) left.tag = "Through"; else left.tag = "Untagged";
    }

    public void UpdateAdjacent(Transform newModule)
    {
        ModuleScript ms = newModule.GetComponent<ModuleScript>();
        //new module is top
        if(baseEditor.ComparePositions(transform.position, newModule.position + new Vector3(0, -4, 0)))
        {
            adjacent[0] = true;
            ms.adjacent[2] = true;
        }
        //new module is right
        if (baseEditor.ComparePositions(transform.position, newModule.position + new Vector3(-5, 0, 0)))
        {
            adjacent[1] = true;
            ms.adjacent[3] = true;
        }
        //new module is bot
        if (baseEditor.ComparePositions(transform.position, newModule.position + new Vector3(0, 4, 0)))
        {
            adjacent[2] = true;
            ms.adjacent[0] = true;
        }
        //new module is left
        if (baseEditor.ComparePositions(transform.position, newModule.position + new Vector3(5, 0, 0)))
        {
            adjacent[3] = true;
            ms.adjacent[1] = true;
        }

        UpdateWallTag();
    }

    public void ResetAdjacents()
    {
        adjacent[0] = false;
        adjacent[1] = false;
        adjacent[2] = false;
        adjacent[3] = false;
        //print("adjacents reseted");
    }
}
