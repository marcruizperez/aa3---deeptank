﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MotorModule : ModuleScript {

    Lever lever;
    
    private int lastLeverState = 0;

    [SerializeField] private static float energyLoss = 2f;


    protected override void Start()
    {
        base.Start();
        lever = transform.GetChild(4).GetComponent<Lever>();
        //Debug.Log("Start function from ChildClass: MotorModule.cs");
    }

    protected override void Update()
    {
        base.Update();
        
        if (lever.leverMoved)
        {
            //change submarine speed index
            //new - last
            submarine.xMovementInputs += lever.state - lastLeverState;
            lastLeverState = lever.state;
            lever.leverMoved = false;
        }

        submarine.totalEnergy -= energyLoss * Mathf.Abs(lastLeverState) * Time.deltaTime;

        
    }

    public override void ResetModule()
    {
        Debug.Log("Module reseted from child class");
    }
}
