﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumpIcon : MonoBehaviour {

    RectTransform rectTrans;
    float sineValue;
    float sizeMultiplier = 0.3f;
    Vector3 originalScale;

	// Use this for initialization
	void Start () {
        rectTrans = GetComponent<RectTransform>();
        originalScale = rectTrans.localScale;
	}
	
	// Update is called once per frame
	void Update () {
        sineValue = Mathf.Sin(Time.time*5f);
        sineValue *= sizeMultiplier; //-0.5 to 0.5

        Vector3 scaleVec = originalScale;
        scaleVec.x += sineValue;
        scaleVec.y += sineValue;

        rectTrans.localScale = scaleVec;
	}
}
