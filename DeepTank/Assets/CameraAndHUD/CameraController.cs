﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


    private Transform subT; //the transform of the submarine
    private submarineScript subC; //the script object instance of the submarine
    private Transform centerOfSubmarine;

    private Camera cam;
    [Header ("Size Properties")]
    [SerializeField] private float sightMarginMultiplier = 3f; //the bigger this value, the more ocean it's seen
    [SerializeField] private float maxSize = 40f;
    [SerializeField] private float minSize = 10f;

	// Use this for initialization
	void Start () {
        subT = GameObject.Find("Submarine").transform;
        subC = subT.GetComponent<submarineScript>();
        centerOfSubmarine = subT.Find("Center");

        cam = GetComponent<Camera>();

        
    }
	
	// Update is called once per frame
	void Update () {
        
        transform.position = new Vector3(centerOfSubmarine.position.x, centerOfSubmarine.position.y, transform.position.z);
        
    }

    public void recalculateCamera()
    {
        Vector2 subRadius = subC.getSubmarineSize();
        resizeCamera(subRadius);
    }

    private void resizeCamera(Vector2 rads)
    {
        rads.x /= cam.aspect; //this maps the value in a 1:1 aspect ratio, which makes easier to calculate
        float biggestRadius = Mathf.Max(rads.x, rads.y);

        float newCameraSize = Mathf.Clamp(biggestRadius* sightMarginMultiplier, minSize, maxSize);
        cam.orthographicSize = newCameraSize;
    }
}
