﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class baseEditor : MonoBehaviour {

    private submarineScript submarine;
    private Transform subCenter;
    public GameObject editorIcon; 
    Transform player1, player2;
    [SerializeField] Transform newModulePreview;
    bool modulePreviewIsReady;
    MeshRenderer meshPreview;
    Color myYellow = new Color(1, 1, 0, 0.65f);
    Color myRed = new Color(1, 0, 0, 0.65f);

    private float radiusEffect = 20f;

    public static bool editorOn = false;
    private bool editorOpenThisFrame = false;

    //modules
    public enum ModuleType { Motor, Preassure, Energy, Hook, Tank};
    ModuleScript mod;
    private int newModuleIndex = 0;
    private int newModuleSubindex = 0;
    private int moduleType = 0;
    int numberOfModuleTypes = 5;
    public GameObject motorPref, preassurePref, energyPref, hookPref, tankPref; //prefabs

    //mouse input
    Vector2 mousePos;
    Vector3 mouseGridPos;

    
    
    void Start () {
        submarine = GameObject.Find("Submarine").GetComponent<submarineScript>();
        subCenter = submarine.transform.Find("Center");
        player1 = submarine.transform.Find("Player");
        player2 = submarine.transform.Find("Player 2");

        //newModulePreview = transform.Find("ModulePreview");
        newModulePreview.gameObject.SetActive(false);
        meshPreview = newModulePreview.GetComponent<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

        float distToSub = Vector2.Distance(subCenter.position, transform.position);
		if(distToSub <= radiusEffect)
        {
            //print("in distance");
            if(!editorOn && !editorIcon.activeSelf){
                editorIcon.SetActive(true); //show button icon
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!editorOn)
                {
                    //player cant move. Reset all modules (stop them)
                    player1.GetComponent<Player>().canMove = false;
                    player2.GetComponent<Player>().canMove = false;
                    submarine.ResetAllModules();
                    //open editor
                    editorOn = true;
                    editorOpenThisFrame = true;
                    newModulePreview.gameObject.SetActive(true);
                    editorIcon.SetActive(false);
                    //show hud


                }
            }
        }else if(editorIcon.activeSelf) editorIcon.SetActive(false);

        if (editorOn) {
            //change type with mouse wheel
            int typeAux = (int)Mathf.Repeat((int)Input.mouseScrollDelta.y + moduleType, numberOfModuleTypes);
            if(typeAux != moduleType)
            {
                moduleType = typeAux;
                //instantiate module for preview
                //
                print(moduleType); //or show something else in UI for exemple
            }

            //click
            if (Input.GetMouseButtonDown(0))
            {
                if (modulePreviewIsReady)
                {
                    InstantiateModuleOfType((ModuleType)moduleType);

                    meshPreview.material.color = myRed;
                    modulePreviewIsReady = false;
                }
            }
            
            //mouse
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 moduleOffset = submarine.modules[0].position;
            mousePos -= moduleOffset;
            Vector3 newModulePos = new Vector2(Mathf.Round(mousePos.x / 5f) * 5f, Mathf.Round(mousePos.y / 4f) * 4f) + moduleOffset;
            if (ChangedGridPos(newModulePos))
            {
                mouseGridPos = newModulePos;

                if (IsValidModulePosition(newModulePos))
                {
                    meshPreview.material.color = myYellow;
                    newModulePreview.position = newModulePos;
                    modulePreviewIsReady = true;
                }
                else
                {
                    //newModulePreview.gameObject.SetActive(false);
                    meshPreview.material.color = myRed;
                    newModulePreview.position = newModulePos;
                    modulePreviewIsReady = false;
                }
                
                
            }

            //close editor
            if (Input.GetKeyDown(KeyCode.Space) && !editorOpenThisFrame)
            {
                editorOn = false;
                player1.GetComponent<Player>().canMove = true;
                player2.GetComponent<Player>().canMove = true;

                newModulePreview.gameObject.SetActive(false);

                //hide hud
            }
        }
        editorOpenThisFrame = false;

    }

    private void FindNextPosibleModule()
    {
        mod = submarine.modules[newModuleIndex].GetComponent<ModuleScript>();

        newModuleSubindex++;

        if (newModuleSubindex == 4)
        {
            newModuleSubindex = 0;
            newModuleIndex++;
            if (newModuleIndex == submarine.modules.Count) newModuleIndex = 0;
            mod = submarine.modules[newModuleIndex].GetComponent<ModuleScript>();
            
        }
        
        if (mod.adjacent[newModuleSubindex] == true) FindNextPosibleModule();

        /*
        while (mod.adjacent[newModuleSubindex] == true)
        {
            newModuleSubindex++;

            if (newModuleSubindex == 4)
            {
                newModuleSubindex = 0;
                newModuleIndex++;
                if (newModuleIndex == submarine.modules.Length) newModuleIndex = 0;
                mod = submarine.modules[newModuleIndex].GetComponent<ModuleScript>();
            }
        }
        */

        //draw preview
        newModulePreview.position = mod.transform.position;
        if (newModuleSubindex == 0) newModulePreview.Translate(0, 4, -5);
        else if (newModuleSubindex == 1) newModulePreview.Translate(5, 0, -5);
        else if (newModuleSubindex == 2) newModulePreview.Translate(0, -4, -5);
        else if (newModuleSubindex == 3) newModulePreview.Translate(-5, 0, -5);
    }

    private void FindPreviousPosibleModule()
    {
        mod = submarine.modules[newModuleIndex].GetComponent<ModuleScript>();

        newModuleSubindex--;

        if (newModuleSubindex == -1)
        {
            newModuleSubindex = 3;
            newModuleIndex--;
            if (newModuleIndex == -1) newModuleIndex = submarine.modules.Count-1;
            mod = submarine.modules[newModuleIndex].GetComponent<ModuleScript>();
        }
        
        if (mod.adjacent[newModuleSubindex] == true) FindPreviousPosibleModule(); //recursive search

        //draw preview
        newModulePreview.position = mod.transform.position;
        if (newModuleSubindex == 0) newModulePreview.Translate(0, 4, -5);
        else if (newModuleSubindex == 1) newModulePreview.Translate(5, 0, -5);
        else if (newModuleSubindex == 2) newModulePreview.Translate(0, -4, -5);
        else if (newModuleSubindex == 3) newModulePreview.Translate(-5, 0, -5);
    }

    bool IsValidModulePosition(Vector3 v)
    {
        bool isValid = false;

        for(int i = 0; i < submarine.modules.Count; i++)
        {
            Vector3 modPos = submarine.modules[i].position;
            if (ComparePositions(v, modPos)) return false;
            if (isValid == false)
            {
                if (ComparePositions(v + new Vector3(0, 4, 0), modPos)) isValid = true;
                if (ComparePositions(v + new Vector3(0, -4, 0), modPos)) isValid = true;
                if (ComparePositions(v + new Vector3(5, 0, 0), modPos)) isValid = true;
                if (ComparePositions(v + new Vector3(-5, 0, 0), modPos)) isValid = true;
            }
            
        }

        return isValid;
    }

    public static bool ComparePositions(Vector3 v1, Vector3 v2)
    {
        if (Mathf.Approximately(v1.x, v2.x))
        {
            if(Mathf.Approximately(v1.y, v2.y))
            {
                return true;
            }
        }
        return false;
    }

    bool ChangedGridPos(Vector3 newGridPos)
    {
        return !ComparePositions(mouseGridPos, newGridPos);
    }

    void InstantiateModuleOfType(ModuleType type)
    {
        GameObject go = new GameObject();
        switch (type)
        {
            case ModuleType.Motor:
                go = Instantiate(motorPref, submarine.modulesParent);
                break;
            case ModuleType.Preassure:
                go = Instantiate(preassurePref, submarine.modulesParent);
                break;
            case ModuleType.Energy:
                go = Instantiate(energyPref, submarine.modulesParent);
                break;
            case ModuleType.Hook:
                go = Instantiate(hookPref, submarine.modulesParent);
                break;
            case ModuleType.Tank:
                go = Instantiate(tankPref, submarine.modulesParent);
                break;
        }

        go.transform.position = newModulePreview.position; //module position = preview position (beware of Z)
        go.transform.SetParent(submarine.modulesParent); //make it child of submarine>modules...
        submarine.AddModule(go.transform);
    }
}
