﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerGame : MonoBehaviour
{
    // Start is called before the first frame update
    //progression variables
    public static int score;
    public static int level = 1;

    private static int currentExp = 0;
    private static int expToNextLvl = 12;

    public GameObject textGo;
    public GameObject textLvl;
    public static ManagerGame instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        UpdateTextLevel();
        UpdateTextScore();
    }

    public void SetPoints(int _points)
    {
        currentExp += _points;
        UpdateTextScore();
        if (currentExp >= expToNextLvl)
        {
            level += 1;
            UpdateTextLevel();
            expToNextLvl = currentExp * 2;
        }
    }
    
    public void UpdateTextScore()
    {
        Text t = textGo.GetComponent<Text>();
        string newT = currentExp.ToString();
        t.text = newT;
    }

    public void UpdateTextLevel()
    {
        Text t = textLvl.GetComponent<Text>();
        string newLvl = level.ToString();
        t.text = newLvl;
    }
}
