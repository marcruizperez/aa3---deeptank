﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Octopus : Animal {

    [SerializeField] private bool haveRest = false;

    private SpriteRenderer mySprite;

    [Space(10)]
    [Header("ExternColiders")]
    [SerializeField] private Collider2D[] groundCol;
    [SerializeField] private Collider2D closestGroundCol;

    [Space(10)]
    Vector2 ascensionPoint;
    [Header("Vertical move behaviour")]
    [Tooltip("Vertica acclereration when it ascend and descen to the ground")]
    public float verticalAcceleration;
    [SerializeField]private bool isGround;
    [SerializeField]private bool isAscending;

    private Sprite sprite;

    protected override void Start ()
    {
        base.Start();
        animalScore = 6;
        typeAnimal = TypeAnimal.aOctopus;
        state = State.Water;

        //moveAcceleration.y = 0;//el pop nomes fara un desplaçament vertical
        myRB = GetComponent<Rigidbody2D>();
        mySprite = GetComponent<SpriteRenderer>();
        tempCD = moveCD;
        newPos = myRB.position;
        currentAccelration = moveAcceleration;

        //COlliders
        myCol = GetComponent<Collider2D>();
        groundCol = GetAllSeaGroundColliders();
        closestGroundCol = GetClosestGround(groundCol);
        isGround = isTouchigCollider(closestGroundCol);

        isAscending = false;//considerem que comença ja flotan en l'aigua
	}

    Collider2D[] GetAllSeaGroundColliders()//aquesta funcio shaura de fer en el sea manager
    {
        GameObject[] groundGameObjects = GameObject.FindGameObjectsWithTag("Ground");
        Collider2D[] myArrCol2D  = new Collider2D[groundGameObjects.Length];
        int count = 0;
        foreach (GameObject g in groundGameObjects)
        {
            myArrCol2D[count] = g.GetComponent<Collider2D>();
            count += 1;
        }

        return myArrCol2D;
    }

    Collider2D GetClosestGround(Collider2D [] groundCOlliders)
    {
        float shortDist = 99999999f;
        Collider2D col2D = null;
        foreach(Collider2D  c in groundCOlliders)
        {
            float distance = Vector2.Distance(transform.position, c.transform.position);
            if(distance < shortDist)
            {
                shortDist = distance;
                col2D = c;
            }
        }

        return col2D;
    }

    void Update()
    {
        switch (state)
        {
            case State.Water:
                {
                    if(tempCD > 0)
                    {
                        tempCD -= Time.deltaTime;
                        Move();
                    }
                    else
                    {
                        if (!haveRest)
                        {
                            StartCoroutine(Rest());
                        }
                        else
                        {
                            if (isGround)
                            {
                                Ascender();
                            }
                            else
                            {
                                Descender();
                            }
                        }

                    }
                    break;
                }

            case State.Hooked:
                {
                    break;
                }

            case State.Aquarium:
                {
                    break;
                }
        }
    }

    private void FixedUpdate()
    {
        switch (state)
        {
            case State.Water:
                {
                    myRB.MovePosition(newPos);
                    break;
                }

            case State.Baited:
                {
                    myRB.position = new Vector2(gameObject.transform.parent.position.x, gameObject.transform.parent.position.y - 0.5f);//a little offset
                    break;
                }

            case State.Aquarium:
                {
                    break;
                }
        }
    }

    void Move()
    {
        moveSpeed += (currentAccelration * Time.deltaTime);
        if (moveSpeed.x >= maxSpeed.x)
        {
            moveSpeed.x = maxSpeed.x;
        }
        else if(moveSpeed.x <= maxSpeed.x * -1)
        {
            moveSpeed.x = maxSpeed.x * -1;
        }
        haveRest = false;
        newPos += (moveSpeed * Time.deltaTime);
    }

    private IEnumerator Rest()
    {
        Color tmp = mySprite.color;
        tmp.a = 0.1f;
        mySprite.color = tmp;
        yield return new WaitForSeconds(Random.Range(2f, 5f));
        tmp.a = 1;
        mySprite.color = tmp;
        haveRest = true;
    }

    private void Descender()
    {
        closestGroundCol = GetClosestGround(groundCol);

        if(!isTouchigCollider(closestGroundCol))
        {
            Vector2 closestPoint = new Vector2(closestGroundCol.bounds.ClosestPoint(transform.position).x, closestGroundCol.bounds.ClosestPoint(transform.position).y);

            newPos = new Vector2(Mathf.Lerp(transform.position.x, closestPoint.x, currentAccelration.x * Time.deltaTime), Mathf.Lerp(transform.position.y, closestPoint.y, verticalAcceleration * Time.deltaTime));
        }
        else
        {
            isGround = true;
            currentAccelration.x = Random.Range(-moveAcceleration.x, moveAcceleration.x);
            tempCD = moveCD;
            moveSpeed = new Vector2(0,0);
        }
    }

    private void Ascender()
    {
        if (!isAscending)
        {
            ascensionPoint = new Vector2(transform.position.x, transform.position.y + Random.Range(2f, 10f));
            isAscending = true;
        }

        else
        {
            float dist =  Mathf.Abs(myRB.position.y - ascensionPoint.y);
            if (dist >= 0.5)
            {
                newPos = new Vector2(myRB.position.x, Mathf.Lerp(myRB.position.y, ascensionPoint.y, verticalAcceleration * Time.deltaTime));
            }
            else
            {

                isGround = false;
                isAscending = false;
                currentAccelration.x = Random.Range(-moveAcceleration.x, moveAcceleration.x);
                tempCD = moveCD;
                moveSpeed = new Vector2(0,0);

            }
        }
    }
    
    /// <summary>
    /// Calcula la distancia entre el gameObejct y el collider del Ground
    /// </summary>
    /// <param name="collider">Collider més a prop en l'eix vertical del gameObject</param>
    /// <returns>True si esta a la minima distancia establerta("tocan")</returns>
    private bool isTouchigCollider(Collider2D collider)
    {
        float a = Mathf.Abs(myCol.bounds.min.y - collider.bounds.max.y);
        if (a <= 0.25)
        {
            return true;
        }

        return false;
    }
}
