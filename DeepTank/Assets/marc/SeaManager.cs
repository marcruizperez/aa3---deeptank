﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaManager : MonoBehaviour {

    public static SeaManager instanceSeaManager;

    [Header("Player Data")]
    [SerializeField] private GameObject subCenter;
    [Tooltip("Max Distance with player to destroy any object of the sea")]
    public float maxDistansce;
    [Tooltip("Min Distance with player to spawn any object")]
    public float minDistance;

    [Space(10)]
    [Header("Shark Manager")]
    [Tooltip("Shark Model")]
    public GameObject sharkPrefab;
    [Tooltip("Spwaner Shark Pos")]
    private Vector3 sharkSpawner;
    [Space(5)]
    [Tooltip("Current Number of Sharks in the scene")]
    [SerializeField]private int numSharks = 0;
    [Tooltip("List of the Sharks created")]
    [SerializeField]private List<Shark> sharksList = new List<Shark>();
    [Tooltip("Max number of sharks that the Spawns accepts")]
    public int numberMaxSharks;
    [Space(5)]
    [Tooltip("Cooldown to spawn a Shark")]
    public float spawnSharkCD;
    private float currentSpwanSharkCD;
    private float currentDistanceShark;

    [Space(15)]
    [Header("Octopus Manager")]
    [Tooltip("OctopusModel")]
    public GameObject octopusPrefab;
    [Tooltip("Spawner Octopus Pos")]
    private Vector3 octopusSpawner;
    [Space(5)]
    [Tooltip("Curren Number of Octopus in the scene")]
    [SerializeField] private int numOctopus = 0;
    [Tooltip("List of the Octopus created")]
    [SerializeField] private List<Octopus> octopusList = new List<Octopus>();
    [Tooltip("Max number of octopus taht the Spawn accept")]
    public int numberMaxOctopus;
    [Space(5)]
    [Tooltip("Cooldown to spawn a Octopus")]
    public float spawnOctopusCD;
    private float currentSpawnOctopusCD;
    private float currentDistanceOctopus;

    [Space(15)]
    [Header("Fish Manager")]
    [Tooltip("OctopusModel")]
    public GameObject fishPrefab;
    [Tooltip("Spawner Octopus Pos")]
    private Vector3 fishSpawner;
    [Space(5)]
    [Tooltip("Curren Number of Octopus in the scene")]
    [SerializeField] private int numFishes = 0;
    [Tooltip("List of the Octopus created")]
    [SerializeField] private List<Fish> fishesList = new List<Fish>();
    [Tooltip("Max number of octopus taht the Spawn accept")]
    public int numMaxFishes;
    [Space(5)]
    [Tooltip("Cooldown to spawn a Octopus")]
    public float spawnFishesCD;
    private float currentSpawnFishesCD;
    private float currentDistanceFishes;

    private void Awake()
    {
        if(instanceSeaManager == null)
        {
            instanceSeaManager = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start ()
    {
        //SHARK 
        currentSpwanSharkCD = spawnSharkCD;
        //ocotpus
        currentSpawnOctopusCD = spawnOctopusCD;
        //fish
        currentSpawnFishesCD = spawnFishesCD;
	}

    void Update()
    {
        //SHARK
        SharkSpawner();
        if (sharksList.Count > 0)
        {
            SharkDestroyer();
        }

        //OctopusSpawner();
        //if(numOctopus > 0)
        //{
        //    OctopusDestroyer();
        //}

        FishSpawner();
        if (numFishes > 0)
        {
            FishDestroyer();
        }
    }

    #region SHARK
    void SharkSpawner()
    {
        if(sharksList.Count < numberMaxSharks)
        {
            currentSpwanSharkCD -= Time.deltaTime;
            if (currentSpwanSharkCD <= 0)
            {
                Vector2 playerDistMin, playerDistMax;
                float angle;
                if (subCenter.transform.position.y < -5)//la posicio del player ha destar certament enfonsat sino spawnejan a laire
                {
                    angle = Random.Range(0, Mathf.PI * 2);
                    playerDistMin = new Vector2(minDistance * Mathf.Cos(angle), minDistance * Mathf.Sin(angle));
                    playerDistMax = new Vector2(maxDistansce * Mathf.Cos(angle), maxDistansce * Mathf.Sin(angle));
                }
                else
                {
                    angle = Random.Range(Mathf.PI, Mathf.PI * 2);
                    playerDistMin = new Vector2(minDistance * Mathf.Cos(angle), minDistance * Mathf.Sin(angle));
                    playerDistMax = new Vector2(maxDistansce * Mathf.Cos(angle), maxDistansce * Mathf.Sin(angle));
                }
                sharkSpawner = new Vector3(Random.Range(subCenter.transform.position.x + playerDistMin.x, subCenter.transform.position.x + playerDistMax.x),
                    Random.Range(subCenter.transform.position.y + playerDistMin.y, subCenter.transform.position.y + playerDistMax.y), 15);
                //INSTANCIARLO
                GameObject a = Instantiate(sharkPrefab, sharkSpawner, Quaternion.identity);
                numSharks += 1;
                //sharksList.Add()
                sharksList.Add(a.GetComponent<Shark>());
                currentSpwanSharkCD = spawnSharkCD;
            }
        }
    }

    void SharkDestroyer()
    {
        for(int i = 0; i < sharksList.Count; i++)
        {
            Vector2 tempSharkPos = new Vector2(sharksList[i].gameObject.transform.position.x, sharksList[i].gameObject.transform.position.y);
            float dist = Vector2.Distance(tempSharkPos, subCenter.transform.position);
            if (dist >= maxDistansce) {
                Destroy(sharksList[i].gameObject);
                sharksList.RemoveAt(i);
                numSharks -= 1;
            }
        }
    }

    public void SharkState()
    {
        List<HookModule> hooksWithBait = GetHooksWithBait();
        for (int i = 0; i < sharksList.Count; i++)
        {
            if (hooksWithBait.Count > 0)
            {
                HookModule tempHookModule = GetHookModuleMoreNear(sharksList[i].gameObject.transform.position, hooksWithBait);
                if(tempHookModule != null)
                {
                    sharksList[i].SetTargetToSeek(tempHookModule.hook);
                }                    
            }
        }
    }

    List<HookModule> GetHooksWithBait()
    {
        HookModule[] _listWithAllHooks = FindObjectsOfType<HookModule>();
        List<HookModule> hooksWithAnimals = new List<HookModule>();
        for (int i = 0; i < _listWithAllHooks.Length; i++)
        {
            if(_listWithAllHooks[i].hook.thereIsBait == true && _listWithAllHooks[i].hook.GetTheBaitAnimal().GetComponent<Animal>().GetAnimalType() == Animal.TypeAnimal.aFish)//
            {
                hooksWithAnimals.Add(_listWithAllHooks[i]);
            }
        }

        return hooksWithAnimals;
    }

    HookModule GetHookModuleMoreNear(Vector2 _sharkPos, List<HookModule> _list)
    {
        float currDist;
        float tempDist = 99999999f;
        Vector2 tempPos;
        HookModule tempHookModule = null;
        for(int i = 0; i < _list.Count; i++)
        {
            tempPos = _list[i].gameObject.transform.position;
            currDist = Mathf.Abs(Vector2.Distance(_sharkPos, tempPos));
            if (tempDist > currDist && currDist < maxDistansce)
            {
                tempDist = currDist;
                tempHookModule = _list[i];
            }
        }

        return tempHookModule;
    }

    #endregion

    #region Octopus
    void OctopusSpawner()
    {
        if (numOctopus < numberMaxOctopus)
        {
            currentSpawnOctopusCD -= Time.deltaTime;

            if (currentSpawnOctopusCD <= 0)
            {
                Vector2 playerDistMin, playerDistMax;
                if (subCenter.transform.position.y < -minDistance)//la posicio del player ha destar certament enfonsat sino spawnejan a laire
                {
                    float angle = Random.Range(0, 2 * Mathf.PI);
                    playerDistMin = new Vector2(minDistance * Mathf.Cos(angle), minDistance * Mathf.Sin(angle));
                    playerDistMax = new Vector2(maxDistansce * Mathf.Cos(angle), maxDistansce * Mathf.Sin(angle));
                }
                else
                {
                    float angle = Random.Range(Mathf.PI, 2 * Mathf.PI);
                    playerDistMin = new Vector2(minDistance * Mathf.Cos(angle), minDistance * Mathf.Sin(angle));
                    playerDistMax = new Vector2(maxDistansce * Mathf.Cos(angle), maxDistansce * Mathf.Sin(angle));
                }
                octopusSpawner = new Vector3(Random.Range(subCenter.transform.position.x + playerDistMin.x, subCenter.transform.position.x + playerDistMax.x),
                    Random.Range(subCenter.transform.position.y + playerDistMin.y, subCenter.transform.position.y + playerDistMax.y), 15);

                //INSTANCIARLO
                GameObject a = Instantiate(octopusPrefab, octopusSpawner, Quaternion.identity);
                numOctopus += 1;
                //octopus.Add()
                octopusList.Add(a.GetComponent<Octopus>());
                currentSpawnOctopusCD = spawnOctopusCD;
            }
        }
    }

    void OctopusDestroyer()
    {
        for (int i = 0; i < octopusList.Count; i++)
        {
            Vector2 temppOctopusPos = new Vector2(octopusList[i].gameObject.transform.position.x, octopusList[i].gameObject.transform.position.y);
            float dist = Vector2.Distance(temppOctopusPos, subCenter.transform.position);
            if (dist >= maxDistansce)
            {
                Destroy(octopusList[i].gameObject);
                octopusList.RemoveAt(i);
                numOctopus -= 1;
            }
        }

    }

    #endregion

    #region Fish
    void FishSpawner()
    {
        if (numFishes < numMaxFishes)
        {
            currentSpawnFishesCD -= Time.deltaTime;

            if (currentSpawnFishesCD <= 0)
            {
                Vector2 playerDistMin, playerDistMax;
                if (subCenter.transform.position.y < -minDistance)//la posicio del player ha destar certament enfonsat sino spawnejan a laire
                {
                    float angle = Random.Range(0, 2 * Mathf.PI);
                    playerDistMin = new Vector2(minDistance * Mathf.Cos(angle), minDistance * Mathf.Sin(angle));
                    playerDistMax = new Vector2(maxDistansce * Mathf.Cos(angle), maxDistansce * Mathf.Sin(angle));
                }
                else
                {
                    float angle = Random.Range(Mathf.PI, 2 * Mathf.PI);
                    playerDistMin = new Vector2(minDistance * Mathf.Cos(angle), minDistance * Mathf.Sin(angle));
                    playerDistMax = new Vector2(maxDistansce * Mathf.Cos(angle), maxDistansce * Mathf.Sin(angle));
                }
                fishSpawner = new Vector3(Random.Range(subCenter.transform.position.x + playerDistMin.x, subCenter.transform.position.x + playerDistMax.x),
                    Random.Range(subCenter.transform.position.y + playerDistMin.y, subCenter.transform.position.y + playerDistMax.y), 15);

                //INSTANCIARLO
                GameObject a = Instantiate(fishPrefab, fishSpawner, Quaternion.identity);
                numFishes += 1;
                fishesList.Add(a.GetComponent<Fish>());
                currentSpawnFishesCD = spawnFishesCD;
            }
        }
    }

    void FishDestroyer()
    {
        for (int i = 0; i < fishesList.Count; i++)
        {
            Vector2 tempFishPos = new Vector2(fishesList[i].gameObject.transform.position.x, fishesList[i].gameObject.transform.position.y);
            float dist = Vector2.Distance(tempFishPos, subCenter.transform.position);
            if (dist >= maxDistansce)
            {
                Destroy(fishesList[i].gameObject);
                fishesList.RemoveAt(i);
                numFishes -= 1;
            }
        }

    }

    #endregion
}
