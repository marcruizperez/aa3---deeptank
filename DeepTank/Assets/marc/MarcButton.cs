﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcButton : MonoBehaviour {

    private Collider2D myCol;
    public bool isPressed;

	void Start ()
    {
        myCol = GetComponent<Collider2D>();
        isPressed = false;
	}

    void OnTriggerEnter2D(Collider2D _col)
    {
        if(_col.gameObject.tag == "Player")// && _col.gameObject.GetComponent<Player>().velocty.y < 0.001)
        {
            if (!isPressed)
            {
                isPressed = true;
            }
        }
    }
	
}
