﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Shark : Animal {

    [Space(5)]
    [Header("Identity Behaviour")]
    [Tooltip("Force Aplied to change the Velocity to Hunt the dished hooked")]
    public float maxForce;
    [SerializeField]private Hook target;
    [SerializeField]private bool havePrey = false;

    protected override void Start()
    {
        base.Start();
        animalScore = 10;
        typeAnimal = TypeAnimal.aShark;
        state = State.Water;

        myRB = GetComponent<Rigidbody2D>();
        myCol = GetComponent<Collider2D>();

        newPos = myRB.position;
        tempCD = moveCD;

        currentAccelration = moveAcceleration;
    }

    void Update()
    {
        switch(state)
        {
            case State.Water:
                {
                    if(!havePrey)
                    {
                        tempCD -= Time.deltaTime;
                        Move();
                    }
                    else//haveprey = true
                    {
                        Seek();
                    }
                    
                    break;
                }

            case State.Hooked:
                {
                    break;
                }

            case State.Aquarium:
                {
                    break;
                }
        }
    }

    private void FixedUpdate()
    {
        switch (state)
        {
            case State.Water:
                {
                    newPos += (moveSpeed * Time.deltaTime);
                    myRB.MovePosition(newPos);
                    break;
                }

            case State.Baited:
                {
                    
                    myRB.position = new Vector2(gameObject.transform.parent.position.x, gameObject.transform.parent.position.y - 0.5f);//a little offset
                    break;
                }

            case State.Aquarium:
                {
                    break;
                }
        }
    }

    private void Move()
    {
        SeaBounds();

        if (tempCD > 0)
        {
            moveSpeed += (currentAccelration * Time.deltaTime);

            if (moveSpeed.x >= maxSpeed.x)
            {
                moveSpeed.x = maxSpeed.x;
            }
            else if (moveSpeed.x <= maxSpeed.x * -1)
            {
                moveSpeed.x = maxSpeed.x * -1;
            }
            else if (moveSpeed.y >= maxSpeed.y)
            {
                moveSpeed.y = maxSpeed.y;
            }
            else if (moveSpeed.y <= maxSpeed.y * -1)
            {
                moveSpeed.y = maxSpeed.y * -1;
            }

        }
        else
        {
            tempCD = moveCD;
            moveSpeed = new Vector2(0,0);
            currentAccelration.x = Random.Range(-moveAcceleration.x, moveAcceleration.x);
            currentAccelration.y = Random.Range(-moveAcceleration.y, moveAcceleration.y);
        }
    }

    private void SeaBounds()
    {
        if (transform.position.y >= 0.2)
        {
            moveSpeed.y *= -1;
        }
    }

    public void SetTargetToSeek(Hook _bait)
    {
        target = _bait;
        havePrey = true;
    }

    private void Seek()
    {
        if (target.GetTheBaitAnimal() != null && target.hookState != Hook.State.Repose)
        {
            Transform baitTarget = target.GetTheBaitAnimal().transform;
            moveSpeed += (currentAccelration * Time.deltaTime);

            if (moveSpeed.x >= maxSpeed.x)
            {
                moveSpeed.x = maxSpeed.x;
            }
            else if (moveSpeed.y >= maxSpeed.y)
            {
                moveSpeed.y = maxSpeed.y;
            }
            Vector2 myPos = new Vector2(transform.position.x, transform.position.y);
            Vector2 posTarget = new Vector2(baitTarget.position.x, baitTarget.position.y);

            Vector2 desiredVelocity = (posTarget - myPos);
            desiredVelocity.Normalize();

            desiredVelocity.x *= maxSpeed.x;
            desiredVelocity.y *= maxSpeed.y;

            Vector2 steeringForce = desiredVelocity - moveSpeed;

            if (steeringForce.x >= maxForce) { steeringForce.x = maxForce; }
            else if (steeringForce.y >= maxForce) { steeringForce.y = maxForce; }

            steeringForce.x /= myRB.mass;
            steeringForce.y /= myRB.mass;


            moveSpeed = moveSpeed + steeringForce;
        }
        else if(target.GetTheBaitAnimal() == null || target.hookState == Hook.State.Repose)
        {
            havePrey = false;
            target = null;
        }

    }

    protected override void OnTriggerEnter2D(Collider2D _col)
    {
        base.OnTriggerEnter2D(_col);
        if(state == State.Water && target != null && _col.gameObject.tag == "Animal")
        {
            if(_col.gameObject.GetComponent<Animal>().GetAnimalState() == State.Baited)
            {
                if (_col.gameObject == target.GetTheBaitAnimal())
                {
                    havePrey = false;
                    target = null;
                    Destroy(_col.gameObject);

                }
            }

        }
    }
}
