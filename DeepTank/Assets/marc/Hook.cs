﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour {

    public enum State { Repose, Ascending, Descending}
    public State hookState;

    [SerializeField]private Vector2 initPos;
    //private Vector2 hookPos;
    [SerializeField]public float minDistance;
    [SerializeField]public float maxDistance;

    public float hookSpeed;
    public bool thereIsBait = false;

    private Rigidbody2D hookRb;
    private Collider2D hookCol;

    private GameObject bait = null;
	void Start ()
    {
        hookState = State.Repose;
        initPos = this.gameObject.transform.parent.position;
        maxDistance = minDistance * maxDistance;
        hookRb = GetComponent<Rigidbody2D>();
        hookCol = GetComponent<Collider2D>();

    }

	void Update ()
    {
        initPos = transform.parent.position;
    }

    private void FixedUpdate()
    {
        switch(hookState)
        {
            case State.Descending:
                {

                    float dist = Vector2.Distance(initPos, transform.position);
                    if (Mathf.Abs(dist) < Mathf.Abs(maxDistance))
                    {
                        hookRb.MovePosition(new Vector2(hookRb.position.x, hookRb.position.y  - hookSpeed *  Time.deltaTime));
                    }
                    else
                    {
                        hookState = State.Ascending;

                    }
                    break;
                }

            case State.Ascending:
                {
                    float dist = Vector2.Distance(initPos, transform.position);
                    if(Mathf.Abs(dist) > Mathf.Abs(minDistance))
                    {
                        hookRb.MovePosition(new Vector2(hookRb.position.x, hookRb.position.y +  hookSpeed * Time.deltaTime));
                    }
                    else
                    {
                        hookState = State.Repose;
                        thereIsBait = false;
                        if(bait != null)
                        {
                            bait.GetComponent<Animal>().SetAnimalState(Animal.State.Hooked);
                        }
                    }
                    break;
                }
        }
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
       if (_col.gameObject.tag == "Animal" && _col.gameObject.GetComponent<Animal>().GetAnimalState() == Animal.State.Water && hookState == State.Descending)
       {
            FindObjectOfType<AudioManager>().PlayAudio("Reward");
            _col.gameObject.transform.parent = this.gameObject.transform;
            _col.gameObject.GetComponent<Animal>().SetAnimalState(Animal.State.Baited);
            _col.gameObject.GetComponent<Animal>().SetHookOfTheAnimal(this);
            if (bait == null)
            {
                thereIsBait = true;
                bait = _col.gameObject;
                bait.transform.position= new Vector3(bait.transform.position.x, bait.transform.position.y, -30f);
                SeaManager.instanceSeaManager.SharkState();
            }           
       }
    }

    public GameObject GetTheBaitAnimal()
    {
        return bait;
    }

    public void RefreshBaitHook()
    {
        bait = null;
        thereIsBait = false;
    }

    public void SetBait(GameObject _b)
    {
        bait = _b;
        thereIsBait = true;
    }

}
