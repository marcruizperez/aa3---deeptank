﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Button : MonoBehaviour {

    public Transform test;

    private enum State { Up, Down};
    private State state;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.tag == "Player" && col.gameObject.GetComponent<Player>().velocity.y < 0)
        {
            FindObjectOfType<AudioManager>().PlayAudio("Button");
            //test.Translate(Vector2.up);
            if (state == State.Up)
            {
                state = State.Down;
                transform.localScale = new Vector3(1, 0.2f, 1);
                float moveAmountY = 0.5f / 2f - 0.2f / 2f;
                transform.Translate(new Vector3(0, -moveAmountY, 0));
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (state == State.Down)
            {
                MakeButtonUp();
            }
        }
    }

    public bool isDown()
    {
        return Convert.ToBoolean(state);
    }

    public void MakeButtonUp()
    {
        transform.localScale = new Vector3(1, 0.5f, 1);
        float moveAmountY = 0.5f / 2f - 0.2f / 2f;
        transform.Translate(new Vector3(0, moveAmountY, 0));
        state = State.Up;
    }
}
