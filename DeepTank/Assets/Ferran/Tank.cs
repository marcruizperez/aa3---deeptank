﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour{

    public bool p1Inside, p2Inside;

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            //player 1
            if(col.GetComponent<Player>().playerID == 1)
            {
                p1Inside = true;
                //print("player 1 in");
            }
            //player 2
            else if (col.GetComponent<Player>().playerID == 2)
            {
                p2Inside = true;
                //print("player 2 in");
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            //player 1
            if (col.GetComponent<Player>().playerID == 1)
            {
                p1Inside = false;
               // print("player 1 out");
            }
            //player 2
            else if (col.GetComponent<Player>().playerID == 2)
            {
                p2Inside = false;
              //  print("player 2 out");
            }
        }
    }
}
