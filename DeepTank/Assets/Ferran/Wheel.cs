﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour {

    bool isBeingUsed = false;
    Player playerUsingWheel;
    
    public float wheelRotMult = 100f;
    public float moveWheelValue;
    Transform sprite;

    // Use this for initialization
    void Start () {
        sprite = transform.GetChild(0);
	}
	
	// Update is called once per frame
	void Update () {
        if (isBeingUsed)
        {
            //check if player leaves
            Vector2 inputs = new Vector2(Input.GetAxisRaw(playerUsingWheel.horizontalAxis), Input.GetAxisRaw(playerUsingWheel.verticalAxis));
            if (inputs.y < 0) //Input.GetKeyDown(playerUsingWheel.upKey)
            {
                playerUsingWheel.canMove = true;
                isBeingUsed = false;
                playerUsingWheel = null;
                moveWheelValue = 0;
            }
            else
            {
                moveWheelValue = inputs.x * Time.deltaTime;
                sprite.Rotate(0, 0, -moveWheelValue * wheelRotMult);
            }
        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" /*&& col.gameObject.GetComponent<Player>().velocity.y != 0*/)
        {
            if (!isBeingUsed)
            {
                playerUsingWheel = col.gameObject.GetComponent<Player>();
                isBeingUsed = true;
                playerUsingWheel.transform.position = transform.position - new Vector3(0, 0.85f, 1);
                playerUsingWheel.canMove = false;
                col.gameObject.GetComponent<Player>().animator.SetTrigger("ActivateWheel");
                col.gameObject.GetComponent<Player>().animator.SetBool("isInWheel", true);
                FindObjectOfType<AudioManager>().PlayAudio("Wheel");
            }
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
       // if (col.tag == "Player" /*&& col.gameObject.GetComponent<Player>().velocity.y != 0*/)
        /*{
            if (!isBeingUsed)
            {
                col.gameObject.GetComponent<Player>().animator.SetTrigger("ActivateWheel");
                col.gameObject.GetComponent<Player>().animator.SetBool("isInWheel", true);
            }
        }*/
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")// && col.gameObject.GetComponent<Player>().playerID == playerUsingWheel.playerID)
        {
            col.gameObject.GetComponent<Player>().animator.SetBool("isInWheel", false);
            FindObjectOfType<AudioManager>().StopAudio("Wheel");
        }
    }
}
