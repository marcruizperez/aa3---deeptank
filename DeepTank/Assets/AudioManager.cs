﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    public static AudioManager instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            DontDestroyOnLoad(gameObject);
        }

        foreach (Sound s in sounds)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.clip;
            s.audioSource.volume = s.volume;
            s.audioSource.panStereo = s.pitch;
            s.audioSource.loop = s.loop;
        }
    }

    private void Start()
    {
        PlayAudio("Theme");
    }
    public void PlayAudio(string _name)
    {
        Sound s = null;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                s = sounds[i];
            }
        }

        if (s == null) { return; }

        s.audioSource.Play();
    }

    public void StopAudio(string _name)
    {
        Sound s = null;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (_name == sounds[i].name)
            {
                s = sounds[i];
            }
        }

        if (s == null) { return; }

        s.audioSource.Stop();
    }
}
